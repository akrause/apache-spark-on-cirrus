# Apache Spark - Fundamentals of Data Management

## Introduction

This repository provides the materials for the practical lab exercises in weeks 8 and 9 of the FDM course.

## Setup

In order to complete the practical exercises (and the assessment on Spark), you will need to have a Spark
environment to work in. There are a variety of methods for setting this up, as described in the sections below.
The simplest way is to use an Anaconda environment on your own PC, and then to launch a 'local' Spark cluster
through the Python API, as explained in method 1. The second method uses Cirrus to launch the Spark cluster and
Jupyter notebook server.

### Method 1 - Local Spark Cluster on Your Own PC
1. Ensure you have the Conda package manager available for managing Python software libraries:
   1. **EITHER** install Anaconda if you want a full Anaconda scientific Python ecosystem and GUI:
      1. Download the latest Anaconda for Windows, Mac or Linux from https://www.anaconda.com/
      2. Follow the installation instructions for your platform at https://docs.anaconda.com/anaconda/install/
   2. **OR** install Miniconda if you only want to use Conda package manager from the command line interface (CLI):
      1. Download the latest Miniconda for Windows, Mac or Linux from https://docs.conda.io/en/latest/miniconda.html
      2. Follow the installation instructions for your platform at https://docs.conda.io/en/latest/miniconda.html#installing
2. Create a new `Python 3.10` virtual environment in
   [Anaconda Navigator](https://docs.anaconda.com/navigator/tutorials/manage-environments/)
   or in [Conda](https://conda.io/projects/conda/en/stable/user-guide/tasks/manage-environments.html), for example:
   ```commandline
   conda create -p <PATH_TO_VIRTUAL_ENV>/spark python=3.10
   ```
3.  Activate the newly created virtual environment:
   ```commandline
   conda activate <PATH_TO_VIRTUAL_ENV>/spark
   ```
4. Install the JupyterLab, Spark, OpenJDK and Numpy packages from the `conda-forge` channel in the Anaconda package
   repository:
   ```commandline
   conda install -c conda-forge jupyterlab pyspark numpy openjdk nltk
   ```
   ***Note***: You may also install the [Mamba](https://mamba.readthedocs.io/en/latest/user_guide/mamba.html) package
   manager **before** you run the above command because it is a lot faster at installing packages than default `conda`.
   To install Mamba, use the command: `conda install -c conda-forge mamba` between steps 3 and 4 above.
5. With these steps complete, clone this repository - `git clone https://git.ecdf.ed.ac.uk/akrause/apache-spark-on-cirrus.git` - then start a Jupyter Notebook server with the following command, and then you can create
   a new `Python 3` kernel notebook with your own code for the assessment or open the existing notebooks for the practical
   (and follow the instructions in [practical-intro.md](./practical-intro.md):
   ```commandline
   jupyter notebook --notebook-dir <PATH_TO_THIS_DIRECTORY>/notebooks
   ```
   This will generate output similar to the following:
   ```commandline
   [I 09:16:56.223 NotebookApp] Serving notebooks from local directory: ./spark_assessment
   [I 09:16:56.223 NotebookApp] Jupyter Notebook 6.5.1 is running at:
   [I 09:16:56.224 NotebookApp] http://localhost:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   [I 09:16:56.225 NotebookApp]  or http://127.0.0.1:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   [I 09:16:56.225 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
   [C 09:16:56.357 NotebookApp]
   
       To access the notebook, open this file in a browser:
           file:///home/user/jupyter/runtime/nbserver-23668-open.html
       Or copy and paste one of these URLs:
           http://localhost:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
        or http://127.0.0.1:8888/?token=b8789a49f6ce64c27d3722c73fb52c93c7ddd5c1c98ef161
   ```
   Copy the token from above and point your local browser to: http://localhost:8888/?token=TOKEN
   (or http://localhost:8888 and type in the token when requsted).
   This should show you a Jupyter home page.    Now you can create a new notebook with Python3 and start the exercise in
   [practical-intro.md](./practical-intro.md).


6. Once you have created or opened a notebook for your code, you may then launch a local Spark cluster - note that the `x` value
   in square brackets in `local[x]` represents the number of cores you wish to allocate to the Spark cluster - with the
   following code in a cell at the top of the notebook, and you can reuse the `spark` and `sc` spark context object for
   all API commands you need:
   ```commandline
   import os
   import sys
    
   os.environ['PYSPARK_PYTHON'] = sys.executable
   os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
    
   # Import SparkSession
   from pyspark.sql import SparkSession, SQLContext
   spark = SparkSession.builder.master("local[4]").appName("FDM_practical").getOrCreate()
   sc = spark.sparkContext

   # sqlContext object is required for Lab 2 notebooks
   sqlContext = SQLContext(spark)
   ```
   
Please find more information about installing and running Jupyter and the Notebook server
[here](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html) and
[here](https://docs.jupyter.org/en/latest/running.html).

#### Bonus Addendum for Mac and Linux Users

It is possible to launch the Jupyter Notebook server and specify the required environment variables to initialise the
Spark session at the same time with the following commands:

```commandline
export SPARK_HOME=<PATH_TO_VIRTUAL_ENV>/lib/python3.10/site-packages/pyspark/
PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_DRIVER_PYTHON_OPTS=notebook pyspark
```
This will initialise the PySpark context with the required paths and launch a notebook server at the same time; you will
therefore no longer need to execute the rather messy code from step *6* above to initialise the Spark session! 

### Method 2 - Spark on Cirrus

Please follow these [Cirrus instructions](./CIRRUS_README.md) for more details on how to setup your Spark environment on Cirrus
You may also refer to the Cirrus [Spark cluster instructions](./cluster/README.md) for more information.

